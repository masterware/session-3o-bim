﻿namespace Session_3_Bim.Telas
{
    partial class frmMenu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.turmasToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem2 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem3 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem4 = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripMenuItem5 = new System.Windows.Forms.ToolStripMenuItem();
            this.maxAlunosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alterarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tipo1ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tipo2ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tipo3ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.removerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tipo1ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tipo2ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.tipo3ToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.alunoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cadastrarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.consultarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.alterarToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.removerToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.ajusteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.trasferidoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.emailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.médiaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ºBimestreToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alterarNotaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.maioresNotasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lblFechar = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.balancearToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.turmasToolStripMenuItem1,
            this.alunoToolStripMenuItem,
            this.médiaToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(451, 33);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // turmasToolStripMenuItem1
            // 
            this.turmasToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarToolStripMenuItem,
            this.consultarToolStripMenuItem,
            this.alterarToolStripMenuItem,
            this.removerToolStripMenuItem,
            this.balancearToolStripMenuItem});
            this.turmasToolStripMenuItem1.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.turmasToolStripMenuItem1.Name = "turmasToolStripMenuItem1";
            this.turmasToolStripMenuItem1.Size = new System.Drawing.Size(86, 29);
            this.turmasToolStripMenuItem1.Text = "Turmas";
            // 
            // cadastrarToolStripMenuItem
            // 
            this.cadastrarToolStripMenuItem.Name = "cadastrarToolStripMenuItem";
            this.cadastrarToolStripMenuItem.Size = new System.Drawing.Size(180, 30);
            this.cadastrarToolStripMenuItem.Text = "Cadastrar";
            this.cadastrarToolStripMenuItem.Click += new System.EventHandler(this.cadastrarToolStripMenuItem_Click);
            // 
            // consultarToolStripMenuItem
            // 
            this.consultarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripMenuItem2,
            this.toolStripMenuItem3,
            this.toolStripMenuItem4,
            this.toolStripMenuItem5,
            this.maxAlunosToolStripMenuItem});
            this.consultarToolStripMenuItem.Name = "consultarToolStripMenuItem";
            this.consultarToolStripMenuItem.Size = new System.Drawing.Size(180, 30);
            this.consultarToolStripMenuItem.Text = "Consultar";
            // 
            // toolStripMenuItem2
            // 
            this.toolStripMenuItem2.Name = "toolStripMenuItem2";
            this.toolStripMenuItem2.Size = new System.Drawing.Size(183, 30);
            this.toolStripMenuItem2.Text = "Tipo 1";
            this.toolStripMenuItem2.Click += new System.EventHandler(this.toolStripMenuItem2_Click);
            // 
            // toolStripMenuItem3
            // 
            this.toolStripMenuItem3.Name = "toolStripMenuItem3";
            this.toolStripMenuItem3.Size = new System.Drawing.Size(183, 30);
            this.toolStripMenuItem3.Text = "Tipo 2";
            this.toolStripMenuItem3.Click += new System.EventHandler(this.toolStripMenuItem3_Click);
            // 
            // toolStripMenuItem4
            // 
            this.toolStripMenuItem4.Name = "toolStripMenuItem4";
            this.toolStripMenuItem4.Size = new System.Drawing.Size(183, 30);
            this.toolStripMenuItem4.Text = "Tipo 3";
            this.toolStripMenuItem4.Click += new System.EventHandler(this.toolStripMenuItem4_Click);
            // 
            // toolStripMenuItem5
            // 
            this.toolStripMenuItem5.Name = "toolStripMenuItem5";
            this.toolStripMenuItem5.Size = new System.Drawing.Size(183, 30);
            this.toolStripMenuItem5.Text = "Tipo 4";
            this.toolStripMenuItem5.Click += new System.EventHandler(this.toolStripMenuItem5_Click);
            // 
            // maxAlunosToolStripMenuItem
            // 
            this.maxAlunosToolStripMenuItem.Name = "maxAlunosToolStripMenuItem";
            this.maxAlunosToolStripMenuItem.Size = new System.Drawing.Size(183, 30);
            this.maxAlunosToolStripMenuItem.Text = "Max Alunos";
            this.maxAlunosToolStripMenuItem.Click += new System.EventHandler(this.maxAlunosToolStripMenuItem_Click);
            // 
            // alterarToolStripMenuItem
            // 
            this.alterarToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tipo1ToolStripMenuItem,
            this.tipo2ToolStripMenuItem,
            this.tipo3ToolStripMenuItem});
            this.alterarToolStripMenuItem.Name = "alterarToolStripMenuItem";
            this.alterarToolStripMenuItem.Size = new System.Drawing.Size(180, 30);
            this.alterarToolStripMenuItem.Text = "Alterar";
            // 
            // tipo1ToolStripMenuItem
            // 
            this.tipo1ToolStripMenuItem.Name = "tipo1ToolStripMenuItem";
            this.tipo1ToolStripMenuItem.Size = new System.Drawing.Size(136, 30);
            this.tipo1ToolStripMenuItem.Text = "Tipo 1";
            this.tipo1ToolStripMenuItem.Click += new System.EventHandler(this.tipo1ToolStripMenuItem_Click);
            // 
            // tipo2ToolStripMenuItem
            // 
            this.tipo2ToolStripMenuItem.Name = "tipo2ToolStripMenuItem";
            this.tipo2ToolStripMenuItem.Size = new System.Drawing.Size(136, 30);
            this.tipo2ToolStripMenuItem.Text = "Tipo 2";
            this.tipo2ToolStripMenuItem.Click += new System.EventHandler(this.tipo2ToolStripMenuItem_Click);
            // 
            // tipo3ToolStripMenuItem
            // 
            this.tipo3ToolStripMenuItem.Name = "tipo3ToolStripMenuItem";
            this.tipo3ToolStripMenuItem.Size = new System.Drawing.Size(136, 30);
            this.tipo3ToolStripMenuItem.Text = "Tipo 3";
            this.tipo3ToolStripMenuItem.Click += new System.EventHandler(this.tipo3ToolStripMenuItem_Click);
            // 
            // removerToolStripMenuItem
            // 
            this.removerToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tipo1ToolStripMenuItem1,
            this.tipo2ToolStripMenuItem1,
            this.tipo3ToolStripMenuItem1});
            this.removerToolStripMenuItem.Name = "removerToolStripMenuItem";
            this.removerToolStripMenuItem.Size = new System.Drawing.Size(180, 30);
            this.removerToolStripMenuItem.Text = "Remover";
            // 
            // tipo1ToolStripMenuItem1
            // 
            this.tipo1ToolStripMenuItem1.Name = "tipo1ToolStripMenuItem1";
            this.tipo1ToolStripMenuItem1.Size = new System.Drawing.Size(136, 30);
            this.tipo1ToolStripMenuItem1.Text = "Tipo 1";
            this.tipo1ToolStripMenuItem1.Click += new System.EventHandler(this.tipo1ToolStripMenuItem1_Click);
            // 
            // tipo2ToolStripMenuItem1
            // 
            this.tipo2ToolStripMenuItem1.Name = "tipo2ToolStripMenuItem1";
            this.tipo2ToolStripMenuItem1.Size = new System.Drawing.Size(136, 30);
            this.tipo2ToolStripMenuItem1.Text = "Tipo 2";
            this.tipo2ToolStripMenuItem1.Click += new System.EventHandler(this.tipo2ToolStripMenuItem1_Click);
            // 
            // tipo3ToolStripMenuItem1
            // 
            this.tipo3ToolStripMenuItem1.Name = "tipo3ToolStripMenuItem1";
            this.tipo3ToolStripMenuItem1.Size = new System.Drawing.Size(136, 30);
            this.tipo3ToolStripMenuItem1.Text = "Tipo 3";
            this.tipo3ToolStripMenuItem1.Click += new System.EventHandler(this.tipo3ToolStripMenuItem1_Click);
            // 
            // alunoToolStripMenuItem
            // 
            this.alunoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.cadastrarToolStripMenuItem1,
            this.consultarToolStripMenuItem1,
            this.alterarToolStripMenuItem1,
            this.removerToolStripMenuItem1,
            this.ajusteToolStripMenuItem,
            this.trasferidoToolStripMenuItem,
            this.emailsToolStripMenuItem});
            this.alunoToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.alunoToolStripMenuItem.Name = "alunoToolStripMenuItem";
            this.alunoToolStripMenuItem.Size = new System.Drawing.Size(82, 29);
            this.alunoToolStripMenuItem.Text = "Alunos";
            // 
            // cadastrarToolStripMenuItem1
            // 
            this.cadastrarToolStripMenuItem1.Name = "cadastrarToolStripMenuItem1";
            this.cadastrarToolStripMenuItem1.Size = new System.Drawing.Size(187, 30);
            this.cadastrarToolStripMenuItem1.Text = "Cadastrar";
            this.cadastrarToolStripMenuItem1.Click += new System.EventHandler(this.cadastrarToolStripMenuItem1_Click);
            // 
            // consultarToolStripMenuItem1
            // 
            this.consultarToolStripMenuItem1.Name = "consultarToolStripMenuItem1";
            this.consultarToolStripMenuItem1.Size = new System.Drawing.Size(187, 30);
            this.consultarToolStripMenuItem1.Text = "Consultar";
            this.consultarToolStripMenuItem1.Click += new System.EventHandler(this.consultarToolStripMenuItem1_Click);
            // 
            // alterarToolStripMenuItem1
            // 
            this.alterarToolStripMenuItem1.Name = "alterarToolStripMenuItem1";
            this.alterarToolStripMenuItem1.Size = new System.Drawing.Size(187, 30);
            this.alterarToolStripMenuItem1.Text = "Alterar";
            this.alterarToolStripMenuItem1.Click += new System.EventHandler(this.alterarToolStripMenuItem1_Click);
            // 
            // removerToolStripMenuItem1
            // 
            this.removerToolStripMenuItem1.Name = "removerToolStripMenuItem1";
            this.removerToolStripMenuItem1.Size = new System.Drawing.Size(187, 30);
            this.removerToolStripMenuItem1.Text = "Remover";
            this.removerToolStripMenuItem1.Click += new System.EventHandler(this.removerToolStripMenuItem1_Click);
            // 
            // ajusteToolStripMenuItem
            // 
            this.ajusteToolStripMenuItem.Name = "ajusteToolStripMenuItem";
            this.ajusteToolStripMenuItem.Size = new System.Drawing.Size(187, 30);
            this.ajusteToolStripMenuItem.Text = "Ajuste";
            this.ajusteToolStripMenuItem.Click += new System.EventHandler(this.ajusteToolStripMenuItem_Click);
            // 
            // trasferidoToolStripMenuItem
            // 
            this.trasferidoToolStripMenuItem.Name = "trasferidoToolStripMenuItem";
            this.trasferidoToolStripMenuItem.Size = new System.Drawing.Size(187, 30);
            this.trasferidoToolStripMenuItem.Text = "Trasferir";
            this.trasferidoToolStripMenuItem.Click += new System.EventHandler(this.trasferidoToolStripMenuItem_Click);
            // 
            // emailsToolStripMenuItem
            // 
            this.emailsToolStripMenuItem.Name = "emailsToolStripMenuItem";
            this.emailsToolStripMenuItem.Size = new System.Drawing.Size(187, 30);
            this.emailsToolStripMenuItem.Text = "Enviar Email";
            this.emailsToolStripMenuItem.Click += new System.EventHandler(this.emailsToolStripMenuItem_Click);
            // 
            // médiaToolStripMenuItem
            // 
            this.médiaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ºBimestreToolStripMenuItem,
            this.alterarNotaToolStripMenuItem,
            this.maioresNotasToolStripMenuItem});
            this.médiaToolStripMenuItem.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.médiaToolStripMenuItem.Name = "médiaToolStripMenuItem";
            this.médiaToolStripMenuItem.Size = new System.Drawing.Size(77, 29);
            this.médiaToolStripMenuItem.Text = "Média";
            // 
            // ºBimestreToolStripMenuItem
            // 
            this.ºBimestreToolStripMenuItem.Name = "ºBimestreToolStripMenuItem";
            this.ºBimestreToolStripMenuItem.Size = new System.Drawing.Size(221, 30);
            this.ºBimestreToolStripMenuItem.Text = "Consultar";
            this.ºBimestreToolStripMenuItem.Click += new System.EventHandler(this.ºBimestreToolStripMenuItem_Click);
            // 
            // alterarNotaToolStripMenuItem
            // 
            this.alterarNotaToolStripMenuItem.Name = "alterarNotaToolStripMenuItem";
            this.alterarNotaToolStripMenuItem.Size = new System.Drawing.Size(221, 30);
            this.alterarNotaToolStripMenuItem.Text = "Alterar Nota";
            this.alterarNotaToolStripMenuItem.Click += new System.EventHandler(this.alterarNotaToolStripMenuItem_Click);
            // 
            // maioresNotasToolStripMenuItem
            // 
            this.maioresNotasToolStripMenuItem.Name = "maioresNotasToolStripMenuItem";
            this.maioresNotasToolStripMenuItem.Size = new System.Drawing.Size(221, 30);
            this.maioresNotasToolStripMenuItem.Text = "3 Maiores Notas";
            this.maioresNotasToolStripMenuItem.Click += new System.EventHandler(this.maioresNotasToolStripMenuItem_Click);
            // 
            // lblFechar
            // 
            this.lblFechar.AutoSize = true;
            this.lblFechar.Font = new System.Drawing.Font("Segoe UI", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFechar.ForeColor = System.Drawing.Color.DarkRed;
            this.lblFechar.Location = new System.Drawing.Point(428, -6);
            this.lblFechar.Name = "lblFechar";
            this.lblFechar.Size = new System.Drawing.Size(27, 30);
            this.lblFechar.TabIndex = 2;
            this.lblFechar.Text = "X";
            this.lblFechar.Click += new System.EventHandler(this.lblFechar_Click);
            this.lblFechar.MouseEnter += new System.EventHandler(this.lblFechar_MouseEnter);
            this.lblFechar.MouseLeave += new System.EventHandler(this.lblFechar_MouseLeave);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::Session_3_Bim.Properties.Resources.logo_NSF;
            this.pictureBox1.Location = new System.Drawing.Point(-1, 37);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(453, 476);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // balancearToolStripMenuItem
            // 
            this.balancearToolStripMenuItem.Name = "balancearToolStripMenuItem";
            this.balancearToolStripMenuItem.Size = new System.Drawing.Size(180, 30);
            this.balancearToolStripMenuItem.Text = "Balancear";
            this.balancearToolStripMenuItem.Click += new System.EventHandler(this.balancearToolStripMenuItem_Click);
            // 
            // frmMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(451, 510);
            this.Controls.Add(this.lblFechar);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.menuStrip1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "frmMenu";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "frmMenu";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem turmasToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem;
        private System.Windows.Forms.Label lblFechar;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem2;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem3;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem4;
        private System.Windows.Forms.ToolStripMenuItem toolStripMenuItem5;
        private System.Windows.Forms.ToolStripMenuItem alterarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem removerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tipo1ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tipo2ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tipo3ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tipo1ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem tipo2ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem tipo3ToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem alunoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cadastrarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem consultarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem alterarToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem removerToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem ajusteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem trasferidoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem médiaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ºBimestreToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem alterarNotaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem maioresNotasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem maxAlunosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem emailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem balancearToolStripMenuItem;
    }
}