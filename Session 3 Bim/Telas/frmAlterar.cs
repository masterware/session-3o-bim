﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Session_3_Bim.Telas
{
    public partial class frmAlterar : Form
    {
        public frmAlterar()
        {
            InitializeComponent();
        }

        private void imgBack_Click(object sender, EventArgs e)
        {
            frmMenu Tela = new frmMenu();
            Tela.Show();
            Hide();
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void lblFechar_MouseEnter(object sender, EventArgs e)
        {
            lblFechar.ForeColor = Color.Red;
        }

        private void lblFechar_MouseLeave(object sender, EventArgs e)
        {
            lblFechar.ForeColor = Color.DarkRed;
        }

        private void imgSearch_Click(object sender, EventArgs e)
        {
            try
            {
                int id = Convert.ToInt32(txtID.Text);

                Business.TurmaBusiness busturma = new Business.TurmaBusiness();
                Model.Entity.tb_turma turma = busturma.ConsultarComID(id);

                txtCurso.Text = turma.nm_curso;
                txtTurma.Text = turma.nm_turma;
                nudMaxAlunos.Value = turma.qt_max_alunos;
            }
            catch (Exception)
            {
                MessageBox.Show("ID inválido", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            Model.Entity.tb_turma turma = new Model.Entity.tb_turma();
            turma.id_turma = Convert.ToInt32(txtID.Text);
            turma.nm_curso = txtCurso.Text;
            turma.nm_turma = txtTurma.Text;
            turma.qt_max_alunos = Convert.ToInt32(nudMaxAlunos.Value);

            Business.TurmaBusiness busturma = new Business.TurmaBusiness();
            busturma.Alterar(turma);

            MessageBox.Show("Turma alterada com sucesso");

            txtCurso.Text = string.Empty;
            txtTurma.Text = string.Empty;
            nudMaxAlunos.Value = 0;
            
        }
    }
}
