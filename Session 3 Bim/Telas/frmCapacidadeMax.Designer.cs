﻿namespace Session_3_Bim.Telas
{
    partial class frmCapacidadeMax
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cboTurma = new System.Windows.Forms.ComboBox();
            this.lblturma = new System.Windows.Forms.Label();
            this.dgvTurma = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTurma)).BeginInit();
            this.SuspendLayout();
            // 
            // cboTurma
            // 
            this.cboTurma.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTurma.FormattingEnabled = true;
            this.cboTurma.Items.AddRange(new object[] {
            "1º Bimestre",
            "2º Bimestre",
            "3º Bimestre ",
            "4º Bimestre"});
            this.cboTurma.Location = new System.Drawing.Point(270, 87);
            this.cboTurma.Name = "cboTurma";
            this.cboTurma.Size = new System.Drawing.Size(121, 21);
            this.cboTurma.TabIndex = 16;
            this.cboTurma.SelectedIndexChanged += new System.EventHandler(this.cboTurma_SelectedIndexChanged);
            // 
            // lblturma
            // 
            this.lblturma.AutoSize = true;
            this.lblturma.Location = new System.Drawing.Point(219, 90);
            this.lblturma.Name = "lblturma";
            this.lblturma.Size = new System.Drawing.Size(45, 13);
            this.lblturma.TabIndex = 15;
            this.lblturma.Text = "Turmas:";
            // 
            // dgvTurma
            // 
            this.dgvTurma.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTurma.Location = new System.Drawing.Point(12, 203);
            this.dgvTurma.Name = "dgvTurma";
            this.dgvTurma.Size = new System.Drawing.Size(581, 150);
            this.dgvTurma.TabIndex = 14;
            // 
            // frmCapacidadeMax
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(618, 365);
            this.Controls.Add(this.cboTurma);
            this.Controls.Add(this.lblturma);
            this.Controls.Add(this.dgvTurma);
            this.Name = "frmCapacidadeMax";
            this.Text = "frmCapacidadeMax";
            ((System.ComponentModel.ISupportInitialize)(this.dgvTurma)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox cboTurma;
        private System.Windows.Forms.Label lblturma;
        private System.Windows.Forms.DataGridView dgvTurma;
    }
}