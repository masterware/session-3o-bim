﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Session_3_Bim.Telas
{
    public partial class frmAjuste : Form
    {
        public frmAjuste()
        {
            InitializeComponent();


            List<Model.Entity.tb_turma> turmas = new List<Model.Entity.tb_turma>();
            Business.TurmaBusiness busturma = new Business.TurmaBusiness();
            turmas = busturma.ConsultarTodasTurmas();

            cboTurmas.DisplayMember = nameof(Model.Entity.tb_turma.nm_turma);
            cboTurmas.DataSource = turmas;
            Model.Entity.tb_turma all = cboTurmas.SelectedItem as Model.Entity.tb_turma;

            txtCurso.Text = all.nm_curso;
        }

        private void cboTurmas_SelectedIndexChanged(object sender, EventArgs e)
        {
            Model.Entity.tb_turma all = cboTurmas.SelectedItem as Model.Entity.tb_turma;

            txtCurso.Text = all.nm_curso;
        }
        
        private void btnAjustar_Click(object sender, EventArgs e)
        {
            string turma = cboTurmas.Text;

            Model.Entity.tb_turma t = cboTurmas.SelectedItem as Model.Entity.tb_turma;
            Business.AlunoBusiness ba = new Business.AlunoBusiness();
            List<Model.Entity.tb_aluno> aluno = ba.ConsultarPorTurma(t);

            MessageBox.Show("Chamada ajustada");
        }

        private void imgBack_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            Hide();
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void lblFechar_MouseEnter(object sender, EventArgs e)
        {
            lblFechar.ForeColor = Color.Red;
        }

        private void lblFechar_MouseLeave(object sender, EventArgs e)
        {
            lblFechar.ForeColor = Color.DarkRed;
        }
    }
}
