﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Session_3_Bim.Telas
{
    public partial class frmTransferencia : Form
    {
        public frmTransferencia()
        {
            InitializeComponent();

            List<Model.Entity.tb_turma> turmas = new List<Model.Entity.tb_turma>();
            Business.TurmaBusiness busturma = new Business.TurmaBusiness();
            turmas = busturma.ConsultarTodasTurmas();

            cboTurma.DisplayMember = nameof(Model.Entity.tb_turma.nm_turma);
            cboTurma.DataSource = turmas;
        }

        private void btnTransferir_Click(object sender, EventArgs e)
        {
            try
            {
                int id = Convert.ToInt32(nudAluno.Value);

                Model.Entity.tb_turma a = cboTurma.SelectedItem as Model.Entity.tb_turma;

                Business.AlunoBusiness busaluno = new Business.AlunoBusiness();
                busaluno.Transferencia(a,id);

                MessageBox.Show("Aluno transeferido com sucesso");
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "Attention", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void nudAluno_ValueChanged(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(nudAluno.Value);

            Business.AlunoBusiness busaluno = new Business.AlunoBusiness();
            Model.Entity.tb_aluno aluno = busaluno.ConsultaIdAluno(id);

            if (aluno != null)
                txtAluno.Text = aluno.nm_aluno;
            else
                txtAluno.Text = string.Empty;
        }

        private void imgBack_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            Hide();
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void lblFechar_MouseEnter(object sender, EventArgs e)
        {
            lblFechar.ForeColor = Color.Red;
        }

        private void lblFechar_MouseLeave(object sender, EventArgs e)
        {
            lblFechar.ForeColor = Color.DarkRed;
        }
    }
}
