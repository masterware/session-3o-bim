﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Session_3_Bim.Telas
{
    public partial class frm3MaioresMedias : Form
    {
        public frm3MaioresMedias()
        {
            InitializeComponent();

            Business.TurmaBusiness busturma = new Business.TurmaBusiness();

            List<Model.Entity.tb_turma> turmas = turmas = busturma.ConsultarTodasTurmas();

            cboTurma.DisplayMember = nameof(Model.Entity.tb_turma.nm_turma);
            cboTurma.DataSource = turmas;
            cboBim.Text = "BIM1";
        }

        private void cboBim_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.Consultar();
        }

        private void cboTurma_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.Consultar();
        }
        private void Consultar()
        {
            int idTurma = (cboTurma.SelectedItem as Model.Entity.tb_turma).id_turma;
            string bim = cboBim.Text;
            
            Business.MediaBusiness busmedia = new Business.MediaBusiness();
            List<Model.Entity.tb_media> list = busmedia.MaioresMedias(idTurma, bim);

            dgvMedias.DataSource = list;
        }

        private void frm3MaioresMedias_Load(object sender, EventArgs e)
        {

        }
    }
}
