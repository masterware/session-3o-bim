﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Session_3_Bim.Telas
{
    public partial class frmRemoverAluno : Form
    {
        public frmRemoverAluno()
        {
            InitializeComponent();
        }

        private void imgSearch_Click(object sender, EventArgs e)
        {
            try
            {
                int id = Convert.ToInt32(txtIDAluno.Text);

                Business.AlunoBusiness busaluno = new Business.AlunoBusiness();
                Model.Entity.tb_aluno aluno = busaluno.ConsultaIdAluno(id);

                if (aluno != null)
                {
                    txtIDTurma.Text = aluno.id_turma.ToString();
                    txtAluno.Text = aluno.nm_aluno;
                    txtChamada.Text = aluno.nr_chamada.ToString();
                    txtBairro.Text = aluno.ds_bairro;
                    txtMunicipio.Text = aluno.ds_municipio;
                    dtpNasc.Value = aluno.dt_nascimento;
                }
                else
                {
                    txtIDTurma.Text = string.Empty;
                    txtAluno.Text = string.Empty;
                    txtChamada.Text = string.Empty;
                    txtBairro.Text = string.Empty;
                    txtMunicipio.Text = string.Empty;
                    dtpNasc.Value = DateTime.Now;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("ID inválido", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnRemover_Click(object sender, EventArgs e)
        {
            try
            {
                int id = Convert.ToInt32(txtIDAluno.Text);

                Business.AlunoBusiness busaluno = new Business.AlunoBusiness();
                busaluno.RemoverAluno(id);

                MessageBox.Show("Aluno removido com sucesso");

                txtIDTurma.Text = string.Empty;
                txtAluno.Text = string.Empty;
                txtChamada.Text = string.Empty;
                txtBairro.Text = string.Empty;
                txtMunicipio.Text = string.Empty;
                dtpNasc.Value = DateTime.Now;
            }
            catch (Exception)
            {
                MessageBox.Show("ID inválido", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void imgBack_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            Hide();
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void lblFechar_MouseEnter(object sender, EventArgs e)
        {
            lblFechar.ForeColor = Color.Red;
        }

        private void lblFechar_MouseLeave(object sender, EventArgs e)
        {
            lblFechar.ForeColor = Color.DarkRed;
        }
    }
}
