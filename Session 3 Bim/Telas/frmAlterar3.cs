﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Session_3_Bim.Telas
{
    public partial class frmAlterar3 : Form
    {
        public frmAlterar3()
        {
            InitializeComponent();

            List<Model.Entity.tb_turma> turmas = new List<Model.Entity.tb_turma>();
            Business.TurmaBusiness busturma = new Business.TurmaBusiness();
            turmas = busturma.ConsultarTodasTurmas();

            cboSearch.DisplayMember = nameof(Model.Entity.tb_turma.nm_turma);
            cboSearch.DataSource = turmas;
        }

        private void imgBack_Click(object sender, EventArgs e)
        {
            frmMenu Tela = new frmMenu();
            Tela.Show();
            Hide();
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void lblFechar_MouseEnter(object sender, EventArgs e)
        {
            lblFechar.ForeColor = Color.Red;
        }

        private void lblFechar_MouseLeave(object sender, EventArgs e)
        {
            lblFechar.ForeColor = Color.DarkRed;
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                Model.Entity.tb_turma turma = new Model.Entity.tb_turma();
                turma.nm_curso = txtCurso.Text;
                turma.nm_turma = txtTurma.Text;
                turma.qt_max_alunos = Convert.ToInt32(nudMaxAlunos.Value);

                Business.TurmaBusiness busturma = new Business.TurmaBusiness();
                busturma.Alterar2(turma);

                MessageBox.Show("Turma alterada com sucesso");

                txtCurso.Text = string.Empty;
                txtTurma.Text = string.Empty;
                nudMaxAlunos.Value = 0;
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void cboSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Model.Entity.tb_turma turmaescolhida = new Model.Entity.tb_turma();
                turmaescolhida.nm_turma = cboSearch.Text;

                Business.TurmaBusiness busturma = new Business.TurmaBusiness();
                turmaescolhida = busturma.ConsultarTurma(turmaescolhida);

                txtTurma.Text = turmaescolhida.nm_turma;
                txtCurso.Text = turmaescolhida.nm_curso;
                nudMaxAlunos.Value = turmaescolhida.qt_max_alunos;
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
