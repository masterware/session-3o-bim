﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Session_3_Bim.Telas
{
    public partial class frmEnviarEmails : Form
    {
        public frmEnviarEmails()
        {
            InitializeComponent();

            List<Model.Entity.tb_turma> turmas = new List<Model.Entity.tb_turma>();
            Business.TurmaBusiness busturma = new Business.TurmaBusiness();
            turmas = busturma.ConsultarTodasTurmas();

            cboTurma.DisplayMember = nameof(Model.Entity.tb_turma.nm_turma);
            cboTurma.DataSource = turmas;
        }
        
        private void btnEnviar_Click(object sender, EventArgs e)
        {
            try
            {
                int id = (cboTurma.SelectedItem as Model.Entity.tb_turma).id_turma;
                string assunto = txtAssunto.Text;
                string mensagem = txtMensagem.Text;

                Business.AlunoBusiness busaluno = new Business.AlunoBusiness();
                busaluno.DestinatariosDeEmail(id, assunto, mensagem);

                MessageBox.Show("Emails enviados");
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
