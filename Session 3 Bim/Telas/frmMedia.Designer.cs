﻿namespace Session_3_Bim.Telas
{
    partial class frmMedia
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cboTurma = new System.Windows.Forms.ComboBox();
            this.lblturma = new System.Windows.Forms.Label();
            this.dgvTurmas = new System.Windows.Forms.DataGridView();
            this.cboBim = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cboOrdem = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column10 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            ((System.ComponentModel.ISupportInitialize)(this.dgvTurmas)).BeginInit();
            this.SuspendLayout();
            // 
            // cboTurma
            // 
            this.cboTurma.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTurma.FormattingEnabled = true;
            this.cboTurma.Items.AddRange(new object[] {
            "1º Bimestre",
            "2º Bimestre",
            "3º Bimestre ",
            "4º Bimestre"});
            this.cboTurma.Location = new System.Drawing.Point(118, 55);
            this.cboTurma.Name = "cboTurma";
            this.cboTurma.Size = new System.Drawing.Size(121, 21);
            this.cboTurma.TabIndex = 7;
            this.cboTurma.SelectedIndexChanged += new System.EventHandler(this.cboTurma_SelectedIndexChanged);
            // 
            // lblturma
            // 
            this.lblturma.AutoSize = true;
            this.lblturma.Location = new System.Drawing.Point(67, 58);
            this.lblturma.Name = "lblturma";
            this.lblturma.Size = new System.Drawing.Size(45, 13);
            this.lblturma.TabIndex = 6;
            this.lblturma.Text = "Turmas:";
            // 
            // dgvTurmas
            // 
            this.dgvTurmas.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvTurmas.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvTurmas.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column5,
            this.Column4,
            this.Column3,
            this.Column6,
            this.Column7,
            this.Column8,
            this.Column9,
            this.Column10});
            this.dgvTurmas.Location = new System.Drawing.Point(12, 150);
            this.dgvTurmas.Name = "dgvTurmas";
            this.dgvTurmas.Size = new System.Drawing.Size(944, 195);
            this.dgvTurmas.TabIndex = 5;
            // 
            // cboBim
            // 
            this.cboBim.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBim.FormattingEnabled = true;
            this.cboBim.Items.AddRange(new object[] {
            "BIM1",
            "BIM2",
            "BIM3",
            "BIM4"});
            this.cboBim.Location = new System.Drawing.Point(340, 55);
            this.cboBim.Name = "cboBim";
            this.cboBim.Size = new System.Drawing.Size(121, 21);
            this.cboBim.TabIndex = 9;
            this.cboBim.SelectedIndexChanged += new System.EventHandler(this.cboBim_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(284, 58);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "Bimestre:";
            // 
            // cboOrdem
            // 
            this.cboOrdem.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboOrdem.FormattingEnabled = true;
            this.cboOrdem.Items.AddRange(new object[] {
            "Nome",
            "Chamada",
            "Média"});
            this.cboOrdem.Location = new System.Drawing.Point(585, 55);
            this.cboOrdem.Name = "cboOrdem";
            this.cboOrdem.Size = new System.Drawing.Size(121, 21);
            this.cboOrdem.TabIndex = 11;
            this.cboOrdem.SelectedIndexChanged += new System.EventHandler(this.cboOrdem_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(513, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(66, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Ordenar por:";
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "id_aluno";
            this.Column1.HeaderText = "ID Aluno";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.DataPropertyName = "id_media";
            this.Column2.HeaderText = "ID Media";
            this.Column2.Name = "Column2";
            // 
            // Column5
            // 
            this.Column5.DataPropertyName = "Aluno";
            this.Column5.HeaderText = "Aluno";
            this.Column5.Name = "Column5";
            // 
            // Column4
            // 
            this.Column4.DataPropertyName = "Turma";
            this.Column4.HeaderText = "Turma";
            this.Column4.Name = "Column4";
            // 
            // Column3
            // 
            this.Column3.DataPropertyName = "Chamada";
            this.Column3.HeaderText = "Chamada";
            this.Column3.Name = "Column3";
            // 
            // Column6
            // 
            this.Column6.DataPropertyName = "ds_bimestre";
            this.Column6.HeaderText = "Bimestre";
            this.Column6.Name = "Column6";
            // 
            // Column7
            // 
            this.Column7.DataPropertyName = "vl_nota1";
            this.Column7.HeaderText = "Nota 1";
            this.Column7.Name = "Column7";
            // 
            // Column8
            // 
            this.Column8.DataPropertyName = "vl_nota2";
            this.Column8.HeaderText = "Nota 2";
            this.Column8.Name = "Column8";
            // 
            // Column9
            // 
            this.Column9.DataPropertyName = "vl_nota3";
            this.Column9.HeaderText = "Nota 3";
            this.Column9.Name = "Column9";
            // 
            // Column10
            // 
            this.Column10.DataPropertyName = "vl_media";
            this.Column10.HeaderText = "Média";
            this.Column10.Name = "Column10";
            // 
            // frmMedia
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SkyBlue;
            this.ClientSize = new System.Drawing.Size(967, 356);
            this.Controls.Add(this.cboOrdem);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cboBim);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cboTurma);
            this.Controls.Add(this.lblturma);
            this.Controls.Add(this.dgvTurmas);
            this.Name = "frmMedia";
            this.Text = "frmMedia";
            ((System.ComponentModel.ISupportInitialize)(this.dgvTurmas)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cboTurma;
        private System.Windows.Forms.Label lblturma;
        private System.Windows.Forms.DataGridView dgvTurmas;
        private System.Windows.Forms.ComboBox cboBim;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboOrdem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column10;
    }
}