﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Session_3_Bim.Telas
{
    public partial class frmConsultar : Form
    {
        public frmConsultar()
        {
            InitializeComponent();
        }

       
        private void btnConsultar_Click(object sender, EventArgs e)
        {
            string curso = txtCurso.Text;
            int maxalunos = Convert.ToInt32(nudMaxAlunos.Value);

            Business.TurmaBusiness busturma = new Business.TurmaBusiness();
            List<Model.Entity.tb_turma> turma = busturma.Consultar(curso, maxalunos);

            dvgTurmas.DataSource = turma;
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void lblFechar_MouseEnter(object sender, EventArgs e)
        {
            lblFechar.ForeColor = Color.Red;

        }

        private void lblFechar_MouseLeave(object sender, EventArgs e)
        {
            lblFechar.ForeColor = Color.DarkRed;
        }

        private void imgBack_Click(object sender, EventArgs e)
        {
            frmMenu Tela = new frmMenu();
            Tela.Show();
            Hide();
        }
    }
}
