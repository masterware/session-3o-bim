﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Session_3_Bim.Telas
{
    public partial class frmConsultarAluno : Form
    {
        public frmConsultarAluno()
        {
            InitializeComponent();
            Business.AlunoBusiness busaluno = new Business.AlunoBusiness();
            List<Model.Entity.tb_aluno> list = new List<Model.Entity.tb_aluno>();
            list = busaluno.ConsultarNormal();

            dgvAlunos.DataSource = list;
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void imgBack_Click(object sender, EventArgs e)
        {
            frmMenu tela = new frmMenu();
            tela.Show();
            Hide();
        }

        private void lblFechar_MouseEnter(object sender, EventArgs e)
        {
            lblFechar.ForeColor = Color.Red;
        }

        private void lblFechar_MouseLeave(object sender, EventArgs e)
        {
            lblFechar.ForeColor = Color.DarkRed;
        }
    }
}
