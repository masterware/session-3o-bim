﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Session_3_Bim.Telas
{
    public partial class frmMedia : Form
    {
        public frmMedia()
        {
            InitializeComponent();

            Business.TurmaBusiness busturma = new Business.TurmaBusiness();

            List<Model.Entity.tb_turma> turmas =  turmas = busturma.ConsultarTodasTurmas();

            cboTurma.DisplayMember = nameof(Model.Entity.tb_turma.nm_turma);
            cboTurma.DataSource = turmas;
            cboBim.Text = "BIM1";
            cboOrdem.Text = "Nome";
        }

        private void cboTurma_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.Consultar();
        }

        private void cboBim_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.Consultar();
        }

        private void cboOrdem_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.Consultar();
        }

        private void Consultar()
        {
            int idTurma = (cboTurma.SelectedItem as Model.Entity.tb_turma).id_turma;
            string bim = cboBim.Text;
            string ordem = cboOrdem.Text;
            Business.MediaBusiness busmedia = new Business.MediaBusiness();


            dgvTurmas.AutoGenerateColumns = false;
            dgvTurmas.DataSource = busmedia.ConsultaPorIDTurma(idTurma, bim, ordem);
        }
    }
}
