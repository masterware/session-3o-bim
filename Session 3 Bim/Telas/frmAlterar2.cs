﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Session_3_Bim.Telas
{
    public partial class frmAlterar2 : Form
    {
        public frmAlterar2()
        {
            InitializeComponent();
        }

        private void imgBack_Click(object sender, EventArgs e)
        {
            frmMenu Tela = new frmMenu();
            Tela.Show();
            Hide();
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void lblFechar_MouseEnter(object sender, EventArgs e)
        {
            lblFechar.ForeColor = Color.Red;
        }

        private void lblFechar_MouseLeave(object sender, EventArgs e)
        {
            lblFechar.ForeColor = Color.DarkRed;
        }

        private void imgSearch_Click(object sender, EventArgs e)
        {
            try
            {
                Model.Entity.tb_turma turma = new Model.Entity.tb_turma();
                turma.nm_turma = txtSearchTurma.Text.Trim();

                Business.TurmaBusiness busturma = new Business.TurmaBusiness();
                Model.Entity.tb_turma turmas = new Model.Entity.tb_turma();
                turmas = busturma.ConsultarTurma(turma);

                if (turmas != null)
                {
                    txtTurma.Text = turmas.nm_turma;
                    txtCurso.Text = turmas.nm_curso;
                    nudMaxAlunos.Value = turmas.qt_max_alunos;
                }
                else
                {
                    txtTurma.Text = string.Empty;
                    txtCurso.Text = string.Empty;
                    nudMaxAlunos.Value = 0;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            try
            {
                Model.Entity.tb_turma turma = new Model.Entity.tb_turma();
                turma.nm_curso = txtCurso.Text;
                turma.nm_turma = txtTurma.Text;
                turma.qt_max_alunos = Convert.ToInt32(nudMaxAlunos.Value);

                Business.TurmaBusiness busturma = new Business.TurmaBusiness();
                busturma.Alterar2(turma);

                MessageBox.Show("Turma alterada com sucesso");

                txtCurso.Text = string.Empty;
                txtTurma.Text = string.Empty;
                nudMaxAlunos.Value = 0;
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
