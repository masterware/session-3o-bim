﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Session_3_Bim.Telas
{
    public partial class frmAlterarNota : Form
    {
        public frmAlterarNota()
        {
            InitializeComponent();

            cboBim.Text = "BIM1";
        }

        private void nudID_ValueChanged(object sender, EventArgs e)
        {
            int id = Convert.ToInt32(nudID.Value);

            Business.AlunoBusiness busaluno = new Business.AlunoBusiness();
            Model.Entity.tb_aluno aluno = busaluno.ConsultaIdAluno(id);

            if (aluno != null)
                txtAluno.Text = aluno.nm_aluno;
            else
                txtAluno.Text = string.Empty;
        }

        private void CalcularMedia()
        {
            decimal nota1 = nudNota1.Value;
            decimal nota2 = nudNota2.Value;
            decimal nota3 = nudNota3.Value;

            Business.MediaBusiness busmedia = new Business.MediaBusiness();
            nudMedia.Value = busmedia.CalcularMedia(nota1, nota2, nota3);
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            Model.Entity.tb_media media = new Model.Entity.tb_media();
            media.vl_nota1 = nudNota1.Value;
            media.vl_nota2 = nudNota2.Value;
            media.vl_nota3 = nudNota3.Value;
            media.vl_media = nudMedia.Value;
            media.id_aluno = Convert.ToInt32(nudID.Value);
            media.ds_bimestre = cboBim.Text;

            Business.MediaBusiness busmedia = new Business.MediaBusiness();
            busmedia.AlterarMedia(media);

            MessageBox.Show("Média alterada com sucesso");
        }

        private void nudNota1_ValueChanged(object sender, EventArgs e)
        {
            this.CalcularMedia();
        }

        private void nudNota2_ValueChanged(object sender, EventArgs e)
        {
            this.CalcularMedia();
        }

        private void nudNota3_ValueChanged(object sender, EventArgs e)
        {
            this.CalcularMedia();
        }

    }
}
