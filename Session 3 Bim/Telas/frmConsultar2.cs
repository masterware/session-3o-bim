﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Session_3_Bim.Telas
{
    public partial class frmConsultar2 : Form
    {
        public frmConsultar2()
        {
            InitializeComponent();
        }

        private void txtCurso_TextChanged(object sender, EventArgs e)
        {
            string curso = txtCurso.Text;
            Business.TurmaBusiness busturma = new Business.TurmaBusiness();
            List<Model.Entity.tb_turma> turma = busturma.Consultar2(curso);

            dgvTurmas.DataSource = turma;
        }

        private void imgBack_Click_1(object sender, EventArgs e)
        {
            frmMenu Tela = new frmMenu();
            Tela.Show();
            Hide();
        }

        private void lblFechar_MouseEnter_1(object sender, EventArgs e)
        {
            lblFechar.ForeColor = Color.Red;
        }

        private void lblFechar_MouseLeave_1(object sender, EventArgs e)
        {
            lblFechar.ForeColor = Color.DarkRed;
        }

        private void lblFechar_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
