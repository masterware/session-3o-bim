﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Session_3_Bim.Telas
{
    public partial class frmBalancear : Form
    {
        public frmBalancear()
        {
            InitializeComponent();

            Business.TurmaBusiness busturma = new Business.TurmaBusiness();
            List<Model.Entity.tb_turma> cursos = busturma.ConsultarCursos();

            cboCurso.DisplayMember = nameof(Model.Entity.tb_turma.nm_curso);
            cboCurso.DataSource = cursos;
        }
    }
}
