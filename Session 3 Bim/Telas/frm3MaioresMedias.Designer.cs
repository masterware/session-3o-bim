﻿namespace Session_3_Bim.Telas
{
    partial class frm3MaioresMedias
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvMedias = new System.Windows.Forms.DataGridView();
            this.cboBim = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cboTurma = new System.Windows.Forms.ComboBox();
            this.lblturma = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvMedias)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvMedias
            // 
            this.dgvMedias.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvMedias.Location = new System.Drawing.Point(12, 209);
            this.dgvMedias.Name = "dgvMedias";
            this.dgvMedias.Size = new System.Drawing.Size(581, 150);
            this.dgvMedias.TabIndex = 0;
            // 
            // cboBim
            // 
            this.cboBim.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboBim.FormattingEnabled = true;
            this.cboBim.Items.AddRange(new object[] {
            "BIM1",
            "BIM2",
            "BIM3",
            "BIM4"});
            this.cboBim.Location = new System.Drawing.Point(362, 86);
            this.cboBim.Name = "cboBim";
            this.cboBim.Size = new System.Drawing.Size(121, 21);
            this.cboBim.TabIndex = 13;
            this.cboBim.SelectedIndexChanged += new System.EventHandler(this.cboBim_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(306, 89);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 12;
            this.label1.Text = "Bimestre:";
            // 
            // cboTurma
            // 
            this.cboTurma.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cboTurma.FormattingEnabled = true;
            this.cboTurma.Items.AddRange(new object[] {
            "1º Bimestre",
            "2º Bimestre",
            "3º Bimestre ",
            "4º Bimestre"});
            this.cboTurma.Location = new System.Drawing.Point(140, 86);
            this.cboTurma.Name = "cboTurma";
            this.cboTurma.Size = new System.Drawing.Size(121, 21);
            this.cboTurma.TabIndex = 11;
            this.cboTurma.SelectedIndexChanged += new System.EventHandler(this.cboTurma_SelectedIndexChanged);
            // 
            // lblturma
            // 
            this.lblturma.AutoSize = true;
            this.lblturma.Location = new System.Drawing.Point(89, 89);
            this.lblturma.Name = "lblturma";
            this.lblturma.Size = new System.Drawing.Size(45, 13);
            this.lblturma.TabIndex = 10;
            this.lblturma.Text = "Turmas:";
            // 
            // frm3MaioresMedias
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(605, 371);
            this.Controls.Add(this.cboBim);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cboTurma);
            this.Controls.Add(this.lblturma);
            this.Controls.Add(this.dgvMedias);
            this.Name = "frm3MaioresMedias";
            this.Text = "frm3MaioresMedias";
            this.Load += new System.EventHandler(this.frm3MaioresMedias_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvMedias)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvMedias;
        private System.Windows.Forms.ComboBox cboBim;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cboTurma;
        private System.Windows.Forms.Label lblturma;
    }
}