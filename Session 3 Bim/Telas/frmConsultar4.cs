﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Session_3_Bim.Telas
{
    public partial class frmConsultar4 : Form
    {
        public frmConsultar4()
        {
            InitializeComponent();
        }
        
        private void imgBack_Click_1(object sender, EventArgs e)
        {
            frmMenu Tela = new frmMenu();
            Tela.Show();
            Hide();
        }

        private void lblFechar_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void lblFechar_MouseEnter_1(object sender, EventArgs e)
        {
            lblFechar.ForeColor = Color.Red;
        }

        private void lblFechar_MouseLeave_1(object sender, EventArgs e)
        {
            lblFechar.ForeColor = Color.DarkRed;
        }

        private void nudMaxAlunos_ValueChanged(object sender, EventArgs e)
        {
            int maxalunos = Convert.ToInt32(nudMaxAlunos.Value);

            Business.TurmaBusiness busturma = new Business.TurmaBusiness();
            List<Model.Entity.tb_turma> turma = busturma.Consultar4(maxalunos);

            dgvTurmas.DataSource = turma;
        }
    }
}
