﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Session_3_Bim.Telas
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
        }

        private void cadastrarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCadastrar Tela = new frmCadastrar();
            Tela.Show();
            Hide();
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void lblFechar_MouseEnter(object sender, EventArgs e)
        {
            lblFechar.ForeColor = Color.Red;
        }

        private void lblFechar_MouseLeave(object sender, EventArgs e)
        {
            lblFechar.ForeColor = Color.DarkRed;
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            frmConsultar Tela = new frmConsultar();
            Tela.Show();
            Hide();
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            frmConsultar2 Tela = new frmConsultar2();
            Tela.Show();
            Hide();
        }

        private void toolStripMenuItem4_Click(object sender, EventArgs e)
        {
            frmConsultar3 Tela = new frmConsultar3();
            Tela.Show();
            Hide();
        }

        private void toolStripMenuItem5_Click(object sender, EventArgs e)
        {
            frmConsultar4 Tela = new frmConsultar4();
            Tela.Show();
            Hide();
        }

        private void tipo1ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAlterar Tela = new frmAlterar();
            Tela.Show();
            Hide();
        }

        private void tipo2ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAlterar2 Tela = new frmAlterar2();
            Tela.Show();
            Hide();
        }

        private void tipo3ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAlterar3 Tela = new frmAlterar3();
            Tela.Show();
            Hide();
        }

        private void tipo1ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmRemover Tela = new frmRemover();
            Tela.Show();
            Hide();
        }

        private void tipo2ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmRemover2 Tela = new frmRemover2();
            Tela.Show();
            Hide();
        }

        private void tipo3ToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmRemover3 Tela = new frmRemover3();
            Tela.Show();
            Hide();
        }

        private void cadastrarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmCadastrarAluno tela = new frmCadastrarAluno();
            tela.Show();
            Hide();
        }

        private void consultarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmConsultarAluno tela = new frmConsultarAluno();
            tela.Show();
            Hide();
        }

        private void alterarToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmAlterarAluno tela = new frmAlterarAluno();
            tela.Show();
            Hide();
        }

        private void removerToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            frmRemoverAluno tela = new frmRemoverAluno();
            tela.Show();
            Hide();
        }

        private void ajusteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAjuste tela = new frmAjuste();
            tela.Show();
            Hide();
        }

        private void trasferidoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmTransferencia tela = new frmTransferencia();
            tela.Show();
            Hide();
        }

        private void ºBimestreToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmMedia tela = new frmMedia();
            tela.Show();
            Hide();
        }

        private void alterarNotaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmAlterarNota Tela = new frmAlterarNota();
            Tela.Show();
            Hide();
        }

        private void maioresNotasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frm3MaioresMedias telas = new Telas.frm3MaioresMedias();
            telas.Show();
            Hide();
        }

        private void maxAlunosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmCapacidadeMax tela = new frmCapacidadeMax();
            tela.Show();
            Hide();
        }

        private void emailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmEnviarEmails tela = new frmEnviarEmails();
            tela.Show();
            Hide();
        }

        private void balancearToolStripMenuItem_Click(object sender, EventArgs e)
        {
            frmBalancear tela = new frmBalancear();
            tela.Show();
            Hide();
        }
    }
}
