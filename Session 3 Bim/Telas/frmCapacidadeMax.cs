﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Session_3_Bim.Telas
{
    public partial class frmCapacidadeMax : Form
    {
        public frmCapacidadeMax()
        {
            InitializeComponent();

            Business.TurmaBusiness busturma = new Business.TurmaBusiness();

            List<Model.Entity.tb_turma> turmas = turmas = busturma.ConsultarTodasTurmas();

            cboTurma.DisplayMember = nameof(Model.Entity.tb_turma.nm_turma);
            cboTurma.DataSource = turmas;
        }

        private void cboTurma_SelectedIndexChanged(object sender, EventArgs e)
        {
            int id = (cboTurma.SelectedItem as Model.Entity.tb_turma).id_turma;

            Business.TurmaBusiness busturmas = new Business.TurmaBusiness();
            List<Model.Entity.tb_turma> list = busturmas.MaxAlunos(id);

            dgvTurma.DataSource = list;
        }
        
        
    }
}
