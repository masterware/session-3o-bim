﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Session_3_Bim.Telas
{
    public partial class frmCadastrarAluno : Form
    {
        public frmCadastrarAluno()
        {
            InitializeComponent();
        }

        private void lblFechar_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void imgBack_Click(object sender, EventArgs e)
        {
            frmMenu Tela = new frmMenu();
            Tela.Show();
            Hide();
        }

        private void lblFechar_MouseEnter(object sender, EventArgs e)
        {
            lblFechar.ForeColor = Color.Red;
        }

        private void lblFechar_MouseLeave(object sender, EventArgs e)
        {
            lblFechar.ForeColor = Color.DarkRed;
        }

        private void btnCadastrar_Click(object sender, EventArgs e)
        {
            Model.Entity.tb_aluno aluno = new Model.Entity.tb_aluno();
            aluno.id_turma = Convert.ToInt32(txtIDTurma.Text);
            aluno.nm_aluno = txtAluno.Text;
            aluno.nr_chamada = Convert.ToInt32(txtChamada.Text);
            aluno.ds_bairro = txtBairro.Text;
            aluno.ds_municipio = txtMunicipio.Text;
            aluno.dt_nascimento = dtpNasc.Value;

            Business.AlunoBusiness busaluno = new Business.AlunoBusiness();
            busaluno.VerificarCadastro(aluno);

            MessageBox.Show("Cadastro efetuado com sucesso");
        }

        private void groupbox_Enter(object sender, EventArgs e)
        {

        }
    }
}
