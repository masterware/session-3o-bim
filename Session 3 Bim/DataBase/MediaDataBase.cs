﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Session_3_Bim.DataBase
{
    class MediaDataBase
    {
        public List<Model.Entity.tb_media> ConsultarMediaPorNome(int id, string bim)
        {
            Model.Entity.schooldbEntities db = new Model.Entity.schooldbEntities();

            var medias = (
                         from m in db.tb_media
                         join a in db.tb_aluno on m.id_aluno equals a.id_aluno
                         join t in db.tb_turma on a.id_turma equals t.id_turma
                         where a.id_turma == id && m.ds_bimestre == bim
                         select new { m, a, t }
                        )
                        .ToList()
                        .Select(x => new Model.Entity.tb_media
                        {
                            id_aluno = x.m.id_aluno,
                            id_media = x.m.id_media,
                            ds_bimestre = x.m.ds_bimestre,
                            vl_media = x.m.vl_media,
                            vl_nota1 = x.m.vl_nota1,
                            vl_nota2 = x.m.vl_nota2,
                            vl_nota3 = x.m.vl_nota3,
                            Aluno = x.a.nm_aluno,
                            Turma = x.t.nm_turma,
                            Chamada = x.a.nr_chamada
                        }).OrderBy(x => x.Aluno)
                        .ToList();

            return medias;
        }

        public List<Model.Entity.tb_media> ConsultarMediaPorChamada(int id, string bim)
        {
            Model.Entity.schooldbEntities db = new Model.Entity.schooldbEntities();

            var medias = (
                         from m in db.tb_media
                         join a in db.tb_aluno on m.id_aluno equals a.id_aluno
                         join t in db.tb_turma on a.id_turma equals t.id_turma
                         where a.id_turma == id && m.ds_bimestre == bim
                         select new { m, a, t }
                        )
                        .ToList()
                        .Select(x => new Model.Entity.tb_media
                        {
                            id_aluno = x.m.id_aluno,
                            id_media = x.m.id_media,
                            ds_bimestre = x.m.ds_bimestre,
                            vl_media = x.m.vl_media,
                            vl_nota1 = x.m.vl_nota1,
                            vl_nota2 = x.m.vl_nota2,
                            vl_nota3 = x.m.vl_nota3,
                            Aluno = x.a.nm_aluno,
                            Turma = x.t.nm_turma,
                            Chamada = x.a.nr_chamada
                        }).OrderBy(x => x.Chamada)
                        .ToList();

            return medias;
        }

        public List<Model.Entity.tb_media> ConsultarMediaPorMedia(int id, string bim)
        {
            Model.Entity.schooldbEntities db = new Model.Entity.schooldbEntities();

            var medias = (
                         from m in db.tb_media
                         join a in db.tb_aluno on m.id_aluno equals a.id_aluno
                         join t in db.tb_turma on a.id_turma equals t.id_turma
                         where a.id_turma == id && m.ds_bimestre == bim
                         select new { m, a, t }
                        )
                        .ToList()
                        .Select(x => new Model.Entity.tb_media
                        {
                            id_aluno = x.m.id_aluno,
                            id_media = x.m.id_media,
                            ds_bimestre = x.m.ds_bimestre,
                            vl_media = x.m.vl_media,
                            vl_nota1 = x.m.vl_nota1,
                            vl_nota2 = x.m.vl_nota2,
                            vl_nota3 = x.m.vl_nota3,
                            Aluno = x.a.nm_aluno,
                            Turma = x.t.nm_turma,
                            Chamada = x.a.nr_chamada
                        }).OrderBy(x => x.vl_media)
                        .ToList();

            return medias;
        }

        public void AlterarMedia(Model.Entity.tb_media media)
        {
            Model.Entity.schooldbEntities db = new Model.Entity.schooldbEntities();
            Model.Entity.tb_media antigo = db.tb_media.First(m => m.id_aluno == media.id_aluno && m.ds_bimestre == media.ds_bimestre);

            antigo.vl_nota1 = media.vl_nota1;
            antigo.vl_nota2 = media.vl_nota2;
            antigo.vl_nota3 = media.vl_nota3;
            antigo.vl_media = media.vl_media;

            db.SaveChanges();
        }

        public List<Model.Entity.tb_media> MaioresMedias(int id, string bim)
        {
            Model.Entity.schooldbEntities db = new Model.Entity.schooldbEntities();

            var medias = (
                         from m in db.tb_media
                         join a in db.tb_aluno on m.id_aluno equals a.id_aluno
                         join t in db.tb_turma on a.id_turma equals t.id_turma
                         where a.id_turma == id && m.ds_bimestre == bim
                         select new { m, a, t }
                        )
                        .ToList()
                        .Select(x => new Model.Entity.tb_media
                        {
                            id_aluno = x.m.id_aluno,
                            id_media = x.m.id_media,
                            ds_bimestre = x.m.ds_bimestre,
                            vl_media = x.m.vl_media,
                            vl_nota1 = x.m.vl_nota1,
                            vl_nota2 = x.m.vl_nota2,
                            vl_nota3 = x.m.vl_nota3,
                            Aluno = x.a.nm_aluno,
                            Turma = x.t.nm_turma,
                            Chamada = x.a.nr_chamada
                        }).OrderByDescending(x => x.vl_media).Take(3)
                        .ToList();

            return medias;
        }
    }
}
