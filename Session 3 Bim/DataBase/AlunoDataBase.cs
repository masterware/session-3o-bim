﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Session_3_Bim.DataBase
{
    class AlunoDataBase
    {
        public void CadastrarAluno(Model.Entity.tb_aluno aluno)
        {
            Model.Entity.schooldbEntities db = new Model.Entity.schooldbEntities();
            db.tb_aluno.Add(aluno);

            db.SaveChanges();
        }

        public List<Model.Entity.tb_aluno> ConsultarComNada()
        {
            Model.Entity.schooldbEntities db = new Model.Entity.schooldbEntities();
            List<Model.Entity.tb_aluno> list = db.tb_aluno.OrderBy(t => t.nm_aluno).ToList();

            return list;
        }

        public Model.Entity.tb_aluno ConsultarPorIdAluno(int id)
        {
            Model.Entity.schooldbEntities db = new Model.Entity.schooldbEntities();
            Model.Entity.tb_aluno aluno = db.tb_aluno.FirstOrDefault(t => t.id_aluno == id);

            return aluno;
        }

        public void AlterarAluno(Model.Entity.tb_aluno novo)
        {
            Model.Entity.schooldbEntities db = new Model.Entity.schooldbEntities();
            Model.Entity.tb_aluno antigo = db.tb_aluno.First(t => t.id_aluno == novo.id_aluno);

            antigo.id_turma = novo.id_turma;
            antigo.nm_aluno = novo.nm_aluno;
            antigo.nr_chamada = novo.nr_chamada;
            antigo.ds_bairro = novo.ds_bairro;
            antigo.ds_municipio = novo.ds_municipio;
            antigo.dt_nascimento = novo.dt_nascimento;
            antigo.ds_email = novo.ds_email;

            db.SaveChanges();
        }
        
        public void RemoverAluno(int id)
        {
            Model.Entity.schooldbEntities db = new Model.Entity.schooldbEntities();
            Model.Entity.tb_aluno remove = db.tb_aluno.First(t => t.id_aluno == id);

            db.tb_aluno.Remove(remove);
            db.SaveChanges();
        }

        public List<Model.Entity.tb_aluno> ConsultarPelaTurma(Model.Entity.tb_turma turma)
        {
            Model.Entity.schooldbEntities db = new Model.Entity.schooldbEntities();
            List<Model.Entity.tb_aluno> list = db.tb_aluno.Where(t => t.id_turma == turma.id_turma).OrderBy(t => t.nm_aluno).ToList();

            int a = 0;

            foreach (Model.Entity.tb_aluno ajuste in list)
            {
                a++;
                ajuste.nr_chamada = a;
                db.SaveChanges();
            }

            return list;
        }

        public void Transferencia(Model.Entity.tb_turma turma, int id)
        {
            Model.Entity.schooldbEntities db = new Model.Entity.schooldbEntities();
            int a = db.tb_aluno.Where(t=> t.id_turma == turma.id_turma).Max(t => t.nr_chamada);
            a = a + 1;

            Model.Entity.tb_aluno antigo = db.tb_aluno.First(t => t.id_aluno == id);
            antigo.id_turma = turma.id_turma;
            antigo.nr_chamada = a;

            db.SaveChanges();
        }

        public void DestinatariosDeEmail(int id, string assunto,string mensagem)
        {
            Model.Entity.schooldbEntities db = new Model.Entity.schooldbEntities();
            List<Model.Entity.tb_aluno> alunos = db.tb_aluno.Where(a => a.id_turma == id).ToList();

            string destinatario = "";
            API.Email.Gmail EnviarEmail = new API.Email.Gmail();

            foreach (Model.Entity.tb_aluno model in alunos)
            {
                destinatario = model.ds_email;

                if (destinatario != null)
                    EnviarEmail.Enviar(destinatario, assunto, mensagem);
                else
                    destinatario = "";
            }
        }
    
        private Model.Entity.tb_turma Contar(string curso)
        {
            Model.Entity.schooldbEntities db = new Model.Entity.schooldbEntities();
            
            var medias = (
                         from a in db.tb_aluno
                         join t in db.tb_turma on a.id_turma equals t.id_turma
                         where t.nm_curso == curso
                         select new { a, t }
                        )
                        .ToList()
                        .Select(x => new Model.Entity.tb_turma
                        {
                            qtd_aluno = x.a.nm_aluno.Count(),
                            qtd_turmas = db.tb_turma.Where(t => t.nm_curso == curso).Count()
                        }) as Model.Entity.tb_turma
                        ;

            return medias;
        }

        public void balancear(string curso)
        {
            Model.Entity.tb_turma alunos = this.Contar(curso);

            decimal qtdporturma = alunos.qtd_aluno / alunos.qtd_turmas;
            qtdporturma = Math.Truncate(qtdporturma);


        }
    }
}
