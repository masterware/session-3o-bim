﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Session_3_Bim.DataBase
{
    class TurmaDataBase
    {
        public void Inserir(Model.Entity.tb_turma turma)
        {
            Model.Entity.schooldbEntities db = new Model.Entity.schooldbEntities();
            db.tb_turma.Add(turma);

            db.SaveChanges();
        }

        public List<Model.Entity.tb_turma> Consultar(string curso, int maxalunos)
        {
            Model.Entity.schooldbEntities db = new Model.Entity.schooldbEntities();
            List<Model.Entity.tb_turma> turma = db.tb_turma.Where(t => t.nm_curso == curso && t.qt_max_alunos >= maxalunos).ToList();

            return turma;
        }

        public List<Model.Entity.tb_turma> ConsultarComNada()
        {
            Model.Entity.schooldbEntities db = new Model.Entity.schooldbEntities();
            List<Model.Entity.tb_turma> turma = db.tb_turma.ToList();

            return turma;
        }

        public List<Model.Entity.tb_turma> ConsultarComCurso(string curso)
        {
            Model.Entity.schooldbEntities db = new Model.Entity.schooldbEntities();
            List<Model.Entity.tb_turma> turma = db.tb_turma.Where(t => t.nm_curso.Contains(curso)).ToList();

            return turma;
        }

        public List<Model.Entity.tb_turma> ConsultarComTurma(string turma)
        {
            Model.Entity.schooldbEntities db = new Model.Entity.schooldbEntities();
            List<Model.Entity.tb_turma> turmas = db.tb_turma.Where(t => t.nm_turma.Contains(turma)).ToList();

            return turmas;
        }

        public List<Model.Entity.tb_turma> ConsultarComMaxAlunos(int maxalunos)
        {
            Model.Entity.schooldbEntities db = new Model.Entity.schooldbEntities();
            List<Model.Entity.tb_turma> turmas = db.tb_turma.Where(t => t.qt_max_alunos == maxalunos).ToList();

            return turmas;
        }

        public Model.Entity.tb_turma ConsultarComID(int id)
        {
            Model.Entity.schooldbEntities db = new Model.Entity.schooldbEntities();
            Model.Entity.tb_turma turma = db.tb_turma.First(t => t.id_turma == id);

            return turma;
        }

        public void Remover(int id)
        {
            Model.Entity.schooldbEntities db = new Model.Entity.schooldbEntities();
            Model.Entity.tb_turma turma = db.tb_turma.First(t => t.id_turma == id);
            db.tb_turma.Remove(turma);

            db.SaveChanges();
        }

        public void Alterar(Model.Entity.tb_turma turma)
        {
            Model.Entity.schooldbEntities db = new Model.Entity.schooldbEntities();
            Model.Entity.tb_turma antigo = db.tb_turma.First(t => t.id_turma == turma.id_turma);
            antigo.nm_curso = turma.nm_curso;
            antigo.nm_turma = turma.nm_turma;
            antigo.qt_max_alunos = turma.qt_max_alunos;

            db.SaveChanges();
        }

        public Model.Entity.tb_turma ConsultarTurma(Model.Entity.tb_turma turma)
        {
            Model.Entity.schooldbEntities db = new Model.Entity.schooldbEntities();
            Model.Entity.tb_turma turmas = new Model.Entity.tb_turma();

            turmas = db.tb_turma.FirstOrDefault(t => t.nm_turma == turma.nm_turma);

            return turmas;
        }

        public void AlterarTurma(Model.Entity.tb_turma turma)
        {
            Model.Entity.schooldbEntities db = new Model.Entity.schooldbEntities();
            Model.Entity.tb_turma antigo = db.tb_turma.First(t => t.nm_turma == turma.nm_turma);
            antigo.nm_curso = turma.nm_curso;
            antigo.nm_turma = turma.nm_turma;
            antigo.qt_max_alunos = turma.qt_max_alunos;

            db.SaveChanges();
        }

        public void RemoverPorTurma(string turma)
        {
            Model.Entity.schooldbEntities db = new Model.Entity.schooldbEntities();
            Model.Entity.tb_turma turmas = db.tb_turma.First(t => t.nm_turma == turma);
            db.tb_turma.Remove(turmas);

            db.SaveChanges();
        }

        public List<Model.Entity.tb_turma> MaxAluno(int id)
        {
            Model.Entity.schooldbEntities db = new Model.Entity.schooldbEntities();
            
            var list = (
                         from a in db.tb_aluno
                         join t in db.tb_turma on a.id_turma equals t.id_turma
                         where a.id_turma == id
                         select new { a, t }
                        )
                        .ToList()
                        .Select(x => new Model.Entity.tb_turma
                        {
                            id_turma = x.a.id_turma,
                            nm_turma = x.t.nm_turma,
                            nm_curso = x.t.nm_curso,
                            qt_max_alunos = x.t.qt_max_alunos,
                            qtd_aluno = db.tb_aluno.Where(t => t.id_turma == id).Count()
                        })
                        .ToList();

            return list;
        }

        public List<Model.Entity.tb_turma> ConsultarCursos()
        {
            Model.Entity.schooldbEntities db = new Model.Entity.schooldbEntities();

            List<Model.Entity.tb_turma> cursos = (from t in db.tb_turma
                                                  select new { t }
                                                  )
                                                  .ToList()
                                                 .Select(t => new Model.Entity.tb_turma
                                                 {
                                                     nm_curso = t.t.nm_curso
                                                 }).Distinct()
                                                 .ToList();

            return cursos;
        }
    }
}