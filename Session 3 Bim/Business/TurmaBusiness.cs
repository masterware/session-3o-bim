﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Session_3_Bim.Business
{
    class TurmaBusiness
    {
        public void Inserir(Model.Entity.tb_turma turma)
        {
            DataBase.TurmaDataBase db = new DataBase.TurmaDataBase();
            Model.Entity.tb_turma verficar = db.ConsultarTurma(turma);

            if (turma.nm_turma == string.Empty)
                throw new ArgumentException("O campo Turma deve ser preenchido");

            if (turma.nm_curso == string.Empty)
                throw new ArgumentException("O campo Curso deve ser preenchido");

            if (turma.qt_max_alunos <= 25)
                throw new ArgumentException("A quantidade máxima de alunos deve ser maior do que 25");

            if (verficar != null)
                throw new ArgumentException("O nome da turma já existe");

            db.Inserir(turma);
        }

        public List<Model.Entity.tb_turma> Consultar (string curso, int maxalunos)
        {
            DataBase.TurmaDataBase db = new DataBase.TurmaDataBase();
            List<Model.Entity.tb_turma> turma = new List<Model.Entity.tb_turma>();

            if (curso == "" && maxalunos == 0)
                turma = db.ConsultarComNada();
            else
                turma = db.Consultar(curso, maxalunos);

            return turma;
        }

        public List<Model.Entity.tb_turma> Consultar2(string curso)
        {
            DataBase.TurmaDataBase db = new DataBase.TurmaDataBase();
            List<Model.Entity.tb_turma> turma = new List<Model.Entity.tb_turma>();

            turma = db.ConsultarComCurso(curso);

            return turma;
        }

        public List<Model.Entity.tb_turma> Consultar3(string turma)
        {
            DataBase.TurmaDataBase db = new DataBase.TurmaDataBase();
            List<Model.Entity.tb_turma> turmas = new List<Model.Entity.tb_turma>();

            turmas = db.ConsultarComTurma(turma);

            return turmas;
        }

        public List<Model.Entity.tb_turma> Consultar4(int maxalunos)
        {
            DataBase.TurmaDataBase db = new DataBase.TurmaDataBase();
            List<Model.Entity.tb_turma> turmas = new List<Model.Entity.tb_turma>();

            turmas = db.ConsultarComMaxAlunos(maxalunos);

            return turmas;
        }

        public Model.Entity.tb_turma ConsultarComID(int id)
        {
            DataBase.TurmaDataBase dbturma = new DataBase.TurmaDataBase();
            Model.Entity.tb_turma turma = dbturma.ConsultarComID(id);

            return turma;
        }

        public void Remover(int id)
        {
            DataBase.TurmaDataBase dbturma = new DataBase.TurmaDataBase();
            dbturma.Remover(id);
        }

        public void Alterar(Model.Entity.tb_turma turma)
        {
            DataBase.TurmaDataBase dbturma = new DataBase.TurmaDataBase();
            dbturma.Alterar(turma);
        }

        public Model.Entity.tb_turma ConsultarTurma(Model.Entity.tb_turma turma)
        {
            DataBase.TurmaDataBase db = new DataBase.TurmaDataBase();
            turma = db.ConsultarTurma(turma);

            return turma;
        }

        public void Alterar2(Model.Entity.tb_turma turma)
        {
            DataBase.TurmaDataBase db = new DataBase.TurmaDataBase();
            db.AlterarTurma(turma);
        }

        public List<Model.Entity.tb_turma> ConsultarTodasTurmas()
        {
            DataBase.TurmaDataBase db = new DataBase.TurmaDataBase();
            List<Model.Entity.tb_turma> turma = new List<Model.Entity.tb_turma>();

            turma = db.ConsultarComNada();

            return turma;
        }

        public void RemoverPorTurma(string turma)
        {
            DataBase.TurmaDataBase db = new DataBase.TurmaDataBase();
            db.RemoverPorTurma(turma);
        }

        public List<Model.Entity.tb_turma> MaxAlunos(int id)
        {
            DataBase.TurmaDataBase dbturma = new DataBase.TurmaDataBase();
            List<Model.Entity.tb_turma> list = dbturma.MaxAluno(id);

            return list;
        }

        public List<Model.Entity.tb_turma> ConsultarCursos()
        {
            DataBase.TurmaDataBase dbturma = new DataBase.TurmaDataBase();
            List<Model.Entity.tb_turma> cursos = dbturma.ConsultarCursos();

            return cursos;
        }
    }
}
