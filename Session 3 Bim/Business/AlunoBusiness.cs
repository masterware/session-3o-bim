﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Session_3_Bim.Business
{
    class AlunoBusiness
    {
        public void VerificarCadastro(Model.Entity.tb_aluno aluno)
        {
            DataBase.AlunoDataBase dbaluno = new DataBase.AlunoDataBase();
            dbaluno.CadastrarAluno(aluno);
        }

        public List<Model.Entity.tb_aluno> ConsultarNormal()
        {
            DataBase.AlunoDataBase dbaluno = new DataBase.AlunoDataBase();
            List<Model.Entity.tb_aluno> aluno = dbaluno.ConsultarComNada();

            return aluno;
        }

        public Model.Entity.tb_aluno ConsultaIdAluno(int id)
        {
            DataBase.AlunoDataBase dbaluno = new DataBase.AlunoDataBase();
            Model.Entity.tb_aluno aluno = dbaluno.ConsultarPorIdAluno(id);

            return aluno;
        }

        public void AlterarAluno(Model.Entity.tb_aluno aluno)
        {
            DataBase.AlunoDataBase dbaluno = new DataBase.AlunoDataBase();
            dbaluno.AlterarAluno(aluno);
        }

        public void RemoverAluno(int id)
        {
            DataBase.AlunoDataBase dbaluno = new DataBase.AlunoDataBase();
            dbaluno.RemoverAluno(id);
        }

        public List<Model.Entity.tb_aluno> ConsultarPorTurma(Model.Entity.tb_turma t)
        {
            DataBase.AlunoDataBase dbaluno = new DataBase.AlunoDataBase();
            List<Model.Entity.tb_aluno> aluno = dbaluno.ConsultarPelaTurma(t);

            return aluno;
        }

        public void Transferencia(Model.Entity.tb_turma turma,int id)
        {
            DataBase.AlunoDataBase dbaluno = new DataBase.AlunoDataBase();
            DataBase.TurmaDataBase dbturma = new DataBase.TurmaDataBase();

            Model.Entity.tb_aluno alunoescolhido = dbaluno.ConsultarPorIdAluno(id);
            Model.Entity.tb_turma turmadoaluno = dbturma.ConsultarComID(alunoescolhido.id_turma);

            if (turma.nm_curso == turmadoaluno.nm_curso)
                dbaluno.Transferencia(turma, id);
            else
                throw new ArgumentException("Aluno não pode ser transferido para uma turma de curso diferente");
        }

        public void DestinatariosDeEmail(int id, string assunto, string mensagem)
        {
            DataBase.AlunoDataBase dbaluno = new DataBase.AlunoDataBase();
            dbaluno.DestinatariosDeEmail(id, assunto, mensagem);
        }
    }
}
