﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Session_3_Bim.Business
{
    class MediaBusiness
    {
        public List<Model.Entity.tb_media> ConsultaPorIDTurma(int id, string bim, string ordem)
        {
            DataBase.MediaDataBase dbmedia = new DataBase.MediaDataBase();
            List<Model.Entity.tb_media> list = new List<Model.Entity.tb_media>();

            if (ordem == "Nome")
                list = dbmedia.ConsultarMediaPorNome(id, bim);
            else if(ordem == "Chamada")
                list = dbmedia.ConsultarMediaPorChamada(id, bim);
            else if(ordem == "Média")
                list = dbmedia.ConsultarMediaPorMedia(id, bim);

            return list;
        }

        public decimal CalcularMedia(decimal n1, decimal n2, decimal n3)
        {
            decimal media = (n1 + n2 + n3) / 3;
            return media;
        }

        public void AlterarMedia(Model.Entity.tb_media media)
        {
            DataBase.MediaDataBase dbmedia = new DataBase.MediaDataBase();
            dbmedia.AlterarMedia(media);
        }

        public List<Model.Entity.tb_media> MaioresMedias(int id, string bim)
        {
            DataBase.MediaDataBase dbmedia = new DataBase.MediaDataBase();
            List<Model.Entity.tb_media> medias = dbmedia.MaioresMedias(id, bim);

            return medias;
        }
    }
}
